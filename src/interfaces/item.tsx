import IStyle from "./style";

export default interface IItem {
    name: string;
    summary: string;
    description: string[];
    tags: string[];
    match?: number;
    codeUrl?: string;
    liveUrl?: string;
    thumbnailUrl?: string;
    awardsUrls?: {
        where: string,
        url: string
    }[]
}