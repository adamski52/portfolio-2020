export default interface IJunk {
    url: string;
    x: number;
    y: number;
    angle: number;
}
