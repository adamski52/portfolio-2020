import { CSSProperties } from "react";

export default interface IStyle {
    style: CSSProperties;
}