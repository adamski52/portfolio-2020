import React, { MouseEvent } from "react";
import IItem from "../interfaces/item";
import "./ItemIcons.scss";

export interface IItemIconsProps {
    item: IItem;
    onSetSeed: (tag:string) => void;
}

export interface IItemIconsState {
    item: IItem;
}

export default class ItemIcons extends React.Component<IItemIconsProps, IItemIconsState> {
    constructor(props: IItemIconsProps) {
        super(props);

        this.state = {
            item: props.item
        };

        this.onSetSeed = this.onSetSeed.bind(this);
    }

    public static getDerivedStateFromProps(props: IItemIconsProps, state: IItemIconsState) {
        return {
            ...state,
            item: props.item
        };
    }

    private onSetSeed(tag:string) {
        this.props.onSetSeed(tag);
    }

    private renderIcons() {
        return this.state.item.tags.map((tag, index) => {
            return (
                <img key={index} onClick={(e:MouseEvent<HTMLImageElement>) => {
                    e.preventDefault();
                    this.onSetSeed(tag);
                }} alt={tag} src={"img/icons/" + tag + ".png"} title={tag} className="icon" />
            );
        });
    }

    public render() {
        return (
            <div>
                {this.renderIcons()}
            </div>
        );
    }
}