import React, { CSSProperties } from "react";
import IJunk from "../interfaces/junk";
import "./Junk.scss";

export interface IJunkProps {
    junk: IJunk;
}

export interface IJunkState {
    junk: IJunk;
}

export default class Junk extends React.Component<IJunkProps, IJunkState> {
    constructor(props: IJunkProps) {
        super(props);

        this.state = {
            junk: props.junk
        };
    }

    public static getDerivedStateFromProps(props: IJunkProps, state: IJunkState) {
        return {
            ...state,
            junk: props.junk
        };
    }

    public render() {
        return (
            <div className="junk" style={this.getStyle()} data-testid="junk" />
        );
    }

    private getStyle(): CSSProperties {
        return {
            backgroundImage: "url(" + this.state.junk.url + ")",
            top: this.state.junk.x + "%",
            left: this.state.junk.y + "%",
            transform: "rotateZ(" + this.state.junk.angle + "deg)"
        };
    }
}