import React from "react";
import { render } from "@testing-library/react";
import NamePlate from "./NamePlate";

describe("NamePlate", () => {
    it("has a name", () => {
        let component = render(<NamePlate />);
        let element = component.getByText(/Jonathan Adamski/);
        expect(element).toBeInTheDocument();
    });
});