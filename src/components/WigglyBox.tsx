import React from "react";
import IStyle from "../interfaces/style";
import "./WigglyBox.scss";

export interface IWigglyBoxProps {
    style: IStyle;
    className: string;
}

export interface IWigglyBoxState {
    style: IStyle;
    className: string;
}

export default class WigglyBox extends React.Component<IWigglyBoxProps, IWigglyBoxState> {
    constructor(props: IWigglyBoxProps) {
        super(props);

        this.state = {
            className: props.className,
            style: props.style
        };
    }

    public static getDerivedStateFromProps(props: IWigglyBoxProps, state: IWigglyBoxState) {
        return {
            ...state,
            className: props.className,
            style: props.style
        };
    }

    public render() {
        return (
            <div className={this.state.className + " wiggly-box-container"} style={this.state.style.style} >
                {this.props.children}
            </div>
        );
    }
}