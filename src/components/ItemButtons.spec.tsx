import React from "react"
import { render, RenderResult } from "@testing-library/react"
import ItemButtons from "./ItemButtons";
import IItem from "../interfaces/item";

describe("ItemButtons", () => {
    let component: RenderResult;
    let item: IItem;

    beforeEach(() => {
        item = {
            name: "my-name",
            tags: ["tag-1", "tag-2"],
            summary: "lol",
            description: ["wait", "wat"],
            codeUrl: "http://www.code-url.com",
            liveUrl: "http://www.live-url.com",
            thumbnailUrl: "http://www.thumbnail-url.com"
        };
    });

    it("should render with the live button", () => {
        component = render(<ItemButtons item={item} />);

        let element = component.getByText("View Demo");
        expect(element).toBeInTheDocument();
        expect(element.getAttribute("href")).toEqual(item.liveUrl);
    });

    it("should not render a live button if there is no url", () => {
        item.liveUrl = undefined;
        component = render(<ItemButtons item={item} />);

        let element = component.queryByText("View Demo");
        expect(element).not.toBeInTheDocument();
    });

    it("should render with the code button", () => {
        component = render(<ItemButtons item={item} />);

        let element = component.getByText("View Code");
        expect(element).toBeInTheDocument();
        expect(element.getAttribute("href")).toEqual(item.codeUrl);
    });

    it("should not render a code button if there is no url", () => {
        item.codeUrl = undefined;
        component = render(<ItemButtons item={item} />);

        let element = component.queryByText("View Code");
        expect(element).not.toBeInTheDocument();
    });
});