import React from "react"
import { render, RenderResult } from "@testing-library/react"
import IItem from "../interfaces/item";
import RandomService from "../services/RandomService";   
import ActiveItem from "./ActiveItem";

describe("ActiveItem", () => {
    const randomService:RandomService = new RandomService();
    let component: RenderResult;
    let item: IItem;
    let onItemBlur: jest.Mock<any, any>;

    beforeEach(() => {
        onItemBlur = jest.fn();
        item = {
            name: "my-name",
            tags: ["tag-1", "tag-2"],
            summary: "lol",
            description: ["wait", "wat"],
            codeUrl: "http://www.code-url.com",
            liveUrl: "http://www.live-url.com",
            thumbnailUrl: "http://www.thumbnail-url.com"
        };
    });
    
    it("should render with the live button", () => {
        component = render(<ActiveItem onItemBlur={onItemBlur} item={item} />);

        let element = component.getByText("View Demo");
        expect(element).toBeInTheDocument();
        expect(element.getAttribute("href")).toEqual(item.liveUrl);
    });

    it("should not render a live button if there is no url", () => {
        item.liveUrl = undefined;
        component = render(<ActiveItem onItemBlur={onItemBlur} item={item} />);

        let element = component.queryByText("View Demo");
        expect(element).not.toBeInTheDocument();
    });

    it("should render with the code button", () => {
        component = render(<ActiveItem onItemBlur={onItemBlur} item={item} />);

        let element = component.getByText("View Code");
        expect(element).toBeInTheDocument();
        expect(element.getAttribute("href")).toEqual(item.codeUrl);
    });

    it("should not render a code button if there is no url", () => {
        item.codeUrl = undefined;
        component = render(<ActiveItem onItemBlur={onItemBlur} item={item} />);

        let element = component.queryByText("View Code");
        expect(element).not.toBeInTheDocument();
    });

    
    it("should render awards buttons, if any", () => {
        item.awardsUrls = [{
            where: "lolward",
            url: "http://www.lol.com"
        }, {
            where: "watward",
            url: "http://www.wat.com"
        }];

        component = render(<ActiveItem onItemBlur={onItemBlur} item={item} />);

        let element = component.getByText("View lolward");
        expect(element).toBeInTheDocument();
        expect(element.getAttribute("href")).toEqual("http://www.lol.com");

        element = component.getByText("View watward");
        expect(element).toBeInTheDocument();
        expect(element.getAttribute("href")).toEqual("http://www.wat.com");
    });


    it("should not render awards buttons, if none", () => {
        component = render(<ActiveItem onItemBlur={onItemBlur} item={item} />);

        let element = component.queryByText("View lolward");
        expect(element).not.toBeInTheDocument();
    });

    it("should render with the thumbnail", () => {
        component = render(<ActiveItem onItemBlur={onItemBlur} item={item} />);

        let element = component.getByAltText(item.name);
        expect(element).toBeInTheDocument();
        expect(element.getAttribute("src")).toEqual(item.thumbnailUrl);
    });

    it("should not render a thumbnail if there is none", () => {
        item.thumbnailUrl = undefined;
        component = render(<ActiveItem onItemBlur={onItemBlur} item={item} />);

        let element = component.queryByAltText(item.name);
        expect(element).not.toBeInTheDocument();
    });

    it("should render with the summary and description", () => {
        component = render(<ActiveItem onItemBlur={onItemBlur} item={item} />);

        let summaryElement = component.getByText(item.summary);
        expect(summaryElement).toBeInTheDocument();

        let descriptionElement;
        for(let i = 0; i < item.description.length; i++) {
            descriptionElement = component.getByText(item.description[i]);
            expect(descriptionElement).toBeInTheDocument();
        }
    });
});