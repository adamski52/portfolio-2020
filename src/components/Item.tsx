import React, { MouseEvent } from "react";
import IItem from "../interfaces/item";
import "./Item.scss";
import RandomService from "../services/RandomService";
import ItemIcons from "./ItemIcons";

export interface IItemProps {
    item: IItem;
    randomService: RandomService;
    onItemFocus: (item:IItem) => void;
    onSetSeed: (tag:string) => void;
}

export interface IItemState {
    item: IItem;
}

export default class Item extends React.Component<IItemProps, IItemState> {
    constructor(props: IItemProps) {
        super(props);

        this.state = {
            item: props.item
        };

        this.onItemFocus = this.onItemFocus.bind(this);
    }

    private onItemFocus(e:MouseEvent<HTMLDivElement>) {
        e.preventDefault();
        this.props.onItemFocus(this.state.item);
    }

    public static getDerivedStateFromProps(props:IItemProps, state:IItemState) {
        return {
            ...state,
            item: props.item
        };
    }

    public render() {
        return (
            <div className="item" data-testid="item">
                <div className="item-text-container" onClick={this.onItemFocus}>
                    <h2 onClick={this.onItemFocus}>{this.state.item.name}</h2>
                    <p onClick={this.onItemFocus}>{this.state.item.summary}</p>
                </div>
                <div className="item-icon-container">
                    <ItemIcons onSetSeed={this.props.onSetSeed} item={this.state.item} />
                </div>
            </div>
        );
    }
}