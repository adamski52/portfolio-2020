import React from "react"
import { render, RenderResult } from "@testing-library/react"
import IItem from "../interfaces/item";
import ItemIcons from "./ItemIcons";

describe("ItemIcons", () => {
    let component: RenderResult;
    let item: IItem;
    let onSetSeed: jest.Mock<any, any>;

    beforeEach(() => {
        onSetSeed = jest.fn();
        
        item = {
            name: "my-name",
            tags: ["tag-1", "tag-2"],
            summary: "lol",
            description: ["wait", "wat"],
            codeUrl: "http://www.code-url.com",
            liveUrl: "http://www.live-url.com",
            thumbnailUrl: "http://www.thumbnail-url.com"
        };
    });


    it("should render with the tags", () => {
        component = render(<ItemIcons onSetSeed={onSetSeed} item={item} />);

        for(let i = 0; i < item.tags.length; i++) {
            expect(component.getByAltText(item.tags[i])).toBeInTheDocument();
        }
    });
});