import React, { MouseEvent } from "react";
import IItem from "../interfaces/item";
import ItemButtons from "./ItemButtons";
import "./ActiveItem.scss";

export interface IActiveItemProps {
    item: IItem;
    onItemBlur: () => void;
}

export interface IActiveItemState {
    item: IItem;
}

export default class ActiveItem extends React.Component<IActiveItemProps, IActiveItemState> {
    constructor(props: IActiveItemProps) {
        super(props);

        this.state = {
            item: props.item
        };

        this.onItemBlur = this.onItemBlur.bind(this);
    }
    
    private onItemBlur(e:MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.props.onItemBlur();
    }

    public static getDerivedStateFromProps(props:IActiveItemProps, state:IActiveItemState) {
        return {
            ...state,
            item: props.item
        };
    }

    private renderImage() {
        if(!this.state.item.thumbnailUrl) {
            return null;
        }

        return (
            <img src={this.state.item.thumbnailUrl} alt={this.state.item.name} />
        );
    }

    private renderDescription() {
        return this.state.item.description.map((p, index) => {
            return (
                <p key={index}>{p}</p>
            );
        });
    }

    public render() {
        return (
            <div className="active-item">
                <h3>{this.state.item.name}</h3>
                <button className="close" onClick={this.onItemBlur}>x</button>
                <div className="active-item-description-container">
                    {this.renderImage()}
                    <p>{this.state.item.summary}</p>
                    {this.renderDescription()}
                    <ItemButtons item={this.state.item} />
                </div>
            </div>
        );
    }
}