import React from "react";
import "./NamePlate.scss";

export interface INamePlateProps {}
export interface INamePlateState {}

export default class NamePlate extends React.Component<INamePlateProps, INamePlateState> {
    public render() {
        return (
            <h1 className="name-plate">Jonathan Adamski</h1>
        );
    }
}