import React from "react"
import { render, RenderResult } from "@testing-library/react"
import IItem from "../interfaces/item";
import Item from "./Item";
import RandomService from "../services/RandomService";

describe("Item", () => {
    const randomService:RandomService = new RandomService();
    let component: RenderResult;
    let item: IItem;
    let onItemFocus: jest.Mock<any, any>;
    let onSetSeed: jest.Mock<any, any>;

    beforeEach(() => {
        onItemFocus = jest.fn();
        onSetSeed = jest.fn();

        item = {
            name: "my-name",
            tags: ["tag-1", "tag-2"],
            summary: "lol",
            description: ["wait", "wat"],
            codeUrl: "http://www.code-url.com",
            liveUrl: "http://www.live-url.com",
            thumbnailUrl: "http://www.thumbnail-url.com"
        };
    });

    it("should render with the name", () => {
        component = render(<Item randomService={randomService} onSetSeed={onSetSeed} onItemFocus={onItemFocus} item={item} />);

        expect(component.getByText(item.name)).toBeInTheDocument();
    });

    it("should render with the tags", () => {
        component = render(<Item randomService={randomService} onSetSeed={onSetSeed} onItemFocus={onItemFocus} item={item} />);

        for(let i = 0; i < item.tags.length; i++) {
            expect(component.getByAltText(item.tags[i])).toBeInTheDocument();
        }
    });
});