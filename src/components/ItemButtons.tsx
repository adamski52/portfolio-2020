import React from "react";
import IItem from "../interfaces/item";
import "./ItemButtons.scss";

export interface IItemButtonsProps {
    item: IItem;
}

export interface IItemButtonsState {
    item: IItem;
}

export default class ItemButtons extends React.Component<IItemButtonsProps, IItemButtonsState> {
    constructor(props: IItemButtonsProps) {
        super(props);

        this.state = {
            item: props.item
        };
    }

    public static getDerivedStateFromProps(props: IItemButtonsProps, state: IItemButtonsState) {
        return {
            ...state,
            item: props.item
        };
    }

    private renderButton(url: string | undefined, btnType: string, btnText: string) {
        if (!url) {
            return null;
        }

        return (
            <a key={btnText} className={"btn " + btnType} href={url} target="_blank" rel="noopener noreferrer">{btnText}</a>
        );
    }

    private renderAwardsButtons() {
        if(!this.state.item.awardsUrls) {
            return null;
        }
        
        return this.state.item.awardsUrls.map((award) => {
            return this.renderButton(award.url, "award", "View " + award.where);
        });
    }

    public render() {
        return (
            <div className="col-12 item-buttons-container">
                {this.renderAwardsButtons()}
                {this.renderButton(this.state.item.liveUrl, "btn-live", "View Demo")}
                {this.renderButton(this.state.item.codeUrl, "btn-code", "View Code")}
            </div>
        );
    }
}