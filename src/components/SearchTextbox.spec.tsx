import React from "react"
import { render, fireEvent, RenderResult } from "@testing-library/react"
import SearchTextbox from "./SearchTextbox"

describe("SearchTextbox", () => {
    const originalSeed = "lol";
    const newSeed = "wat";
    let onSetSeed: jest.Mock<any, any>;
    let component: RenderResult;
    let element: HTMLElement;

    beforeEach(() => {
        onSetSeed = jest.fn();
        component = render(<SearchTextbox onSetSeed={onSetSeed} seed={originalSeed} />)
        element = component.getByLabelText("wants to show you projects featuring");
    });

    it("should render with a default seed", () => {
        expect(element.getAttribute("value")).toBe("lol");
    });

    it("should call onSetSeed prop on change", () => {
        fireEvent.change(element, {
            target: {
                value: newSeed
            }
        });

        expect(onSetSeed).toHaveBeenCalledWith(newSeed);
    });

    it("should set seed state when props change", () => {
        let actual = SearchTextbox.getDerivedStateFromProps({
            onSetSeed: () => { },
            seed: newSeed
        }, {
            seed: originalSeed
        });

        expect(actual).toEqual({
            seed: newSeed
        });
    });

    it("should start with the textbox focused", () => {
        let focusedItem = document.activeElement;
        expect(focusedItem).toEqual(element);
    });
});