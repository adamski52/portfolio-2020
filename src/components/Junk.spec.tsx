import React from "react"
import { render, RenderResult } from "@testing-library/react"
import Junk from "./Junk"
import IJunk from "../interfaces/junk";

describe("Junk", () => {
    let component: RenderResult;
    let element: HTMLElement;
    let junk: IJunk;

    beforeEach(() => {
        junk = {
            url: "lol",
            x: 0,
            y: 1,
            angle: 2
        };

        component = render(<Junk junk={junk} />)
        element = component.getByTestId("junk");
    });

    it("should render with default props", () => {
        expect(element.getAttribute("style")).toBe("background-image: url(lol); top: 0%; left: 1%; transform: rotateZ(2deg);");
    });

    it("should set state when props change", () => {
        let actual = Junk.getDerivedStateFromProps({
            junk: {
                url: "wat",
                x: 3,
                y: 4,
                angle: 5
            }
        }, {
            junk
        });

        expect(actual).toEqual({
            junk: {
                url: "wat",
                x: 3,
                y: 4,
                angle: 5
            }
        });
    });
});