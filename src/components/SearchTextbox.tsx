import React, { ChangeEvent } from "react";
import "./SearchTextbox.scss";

export interface ISearchTextboxProps {
    onSetSeed: (val: string) => void;
    seed: string;
}

export interface ISearchTextboxState {
    seed: string;
}

export default class SearchTextbox extends React.Component<ISearchTextboxProps, ISearchTextboxState> {
    constructor(props: ISearchTextboxProps) {
        super(props);

        this.state = {
            seed: props.seed || ""
        };

        this.onChange = this.onChange.bind(this);
    }

    public static getDerivedStateFromProps(props: ISearchTextboxProps, state: ISearchTextboxState) {
        return {
            ...state,
            seed: props.seed || ""
        };
    }

    private onChange(e: ChangeEvent<HTMLInputElement>) {
        e.preventDefault();
        this.props.onSetSeed(e.target.value.trim());
    }

    public render() {
        return (
            <div className="row">
                <div className="col-8">
                    <label htmlFor="search">wants to show you projects featuring</label>
                </div>
                <div className="col-4 search-textbox-input-container">
                    <input autoFocus={true} type="text" id="search" defaultValue={this.state.seed} onChange={this.onChange} />
                </div>
            </div>
        );
    }
}