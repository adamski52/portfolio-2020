import IJunk from "../interfaces/junk";
import JunkService from "./JunkService";
import RandomService from "./RandomService";

describe("JunkService", () => {
    const randomService = new RandomService();
    const junkService = new JunkService(randomService);

    const narrowProperties = (junks: IJunk[], property: string) => {
        return junks.map((junk: any) => {
            return {
                [property]: junk[property]
            };
        });
    };

    it("should get junk", () => {
        let actual = junkService.getRandomJunk();
        expect(actual.length).toEqual(14);
    });

    it("should get the same junk but with different properties", () => {
        let actual1 = junkService.getRandomJunk();
        let actual2 = junkService.getRandomJunk();

        let url1 = narrowProperties(actual1, "url");
        let url2 = narrowProperties(actual2, "url");
        expect(url1).toEqual(url2);

        let x1 = narrowProperties(actual1, "x");
        let x2 = narrowProperties(actual2, "x");
        expect(x1).not.toEqual(x2);

        let y1 = narrowProperties(actual1, "y");
        let y2 = narrowProperties(actual2, "y");
        expect(y1).not.toEqual(y2);

        let angle1 = narrowProperties(actual1, "angle");
        let angle2 = narrowProperties(actual2, "angle");
        expect(angle1).not.toEqual(angle2);
    });
});