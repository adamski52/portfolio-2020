import fetch from "cross-fetch";

export default class HttpService {
    private baseUrl = "";

    constructor(baseUrl:string) {
        this.baseUrl = baseUrl;
    }

    private toJson(response:Response) {
        if(response.ok) {
            return response.json();
        }

        throw response;
    }

    public async get(url:string) {
        let response = await fetch(this.baseUrl + url, {
            headers: {
                "Content-Type": "application/json"
            }
        });
        
        return this.toJson(response);
    }

    public async post(url:string, body:any) {
        let response = await fetch(this.baseUrl + url, {
            method: "POST",
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json"
            }
        });

        return this.toJson(response);
    }

    public async put(url:string, body:any) {
        let response = await fetch(this.baseUrl + url, {
            method: "PUT",
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json"
            }
        });
        
        return this.toJson(response);
    }

    public async delete(url:string) {
        let response = await fetch(this.baseUrl + url, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        });

        return this.toJson(response);
    }
}