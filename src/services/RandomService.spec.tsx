import RandomService from "./RandomService";

describe("RandomService", () => {
    const randomService = new RandomService();

    it("should get a random number between two numbers", () => {
        randomService.setSeed("lol");
        let actual = randomService.getRandomBetween(1, 2);
        expect(actual).toEqual(1.1766342262026228);
    });

    it("should switch min and max if they're reversed", () => {
        randomService.setSeed("lol");
        let actual = randomService.getRandomBetween(2, 1);
        expect(actual).toEqual(1.1766342262026228);
    });
});