export default class RandomService {
    private seed: number = 0;

    public setSeed(seed: string) {
        let formattedSeed: string | undefined = seed.trim().toUpperCase();
        if (seed === "") {
            formattedSeed = undefined;
        }

        this.seed = parseInt(formattedSeed + "", 36);
    }

    public seededRandom() {
        let x = Math.sin(this.seed++) * 10000;
        return x - Math.floor(x);
    }

    public getRandomBetween(minNum: number, maxNum: number): number {
        if(minNum > maxNum) {
            return this.getRandomBetween(maxNum, minNum);
        }
        return minNum + this.seededRandom() * (maxNum - minNum);
    }
}
