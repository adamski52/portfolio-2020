import IJunk from "../interfaces/junk";
import RandomService from "./RandomService";

export default class JunkService {
    private numJunks = 14;
    private randomService: RandomService;

    constructor(randomService: RandomService) {
        this.randomService = randomService;
    }

    public getRandomJunk() {
        let junks: IJunk[] = [];

        for (let i = 0; i < this.numJunks; i++) {
            junks.push({
                url: "img/img" + i + ".png",
                x: this.randomService.getRandomBetween(-50, 100),
                y: this.randomService.getRandomBetween(-50, 100),
                angle: this.randomService.getRandomBetween(0, 360)
            });
        }

        return junks;
    }
}