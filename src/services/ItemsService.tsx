import IItem from "../interfaces/item";
import HttpService from "./HttpService";

export default class ItemService {
    private items: IItem[];
    private matchThreshold = .75;

    constructor(httpService:HttpService) {
        this.items = [{
            name: "Portfolio (2018)",
            liveUrl: "http://jonathanadamski.com",
            codeUrl: "https://gitlab.com/adamski52/portfolio-2020",
            thumbnailUrl: "https://gitlab.com/adamski52/portfolio-2020/-/raw/master/thumbnail.jpg",
            summary: "You are here",
            description: [
                "My 2020 portfolio featuring seeded randomness.  This is a redo of a site I made in about 2004 or so.  As you search, a seeded random number is generated which affects the position of all elements on the page.",
                "To be honest, I don't really like this site.  It's just kinda meh.  Nothing special about it."
            ],
            tags: [
                "typescript",
                "react",
                "jest",
                "sass",
                "bootstrap",
                "aws",
                "photoshop",
                "design",
                "development",
                "live"
            ]
        }, {
            name: "Portfolio (2018)",
            liveUrl: "http://jonathanadamski.com/archive/portfolio-2018/",
            codeUrl: "https://gitlab.com/adamski52/flatlay",
            thumbnailUrl: "https://gitlab.com/adamski52/flatlay/-/raw/master/ui/thumbnail.jpg",
            summary: "Try MARIO, ISAAC, DOOM2, ZELDA, VALVE, DRWHO, SONIC and NUKES",
            description: [
                "My 2018 portfolio featuring procedural generation.  There are hidden keys that cause the site to change more than a standard date change does.",
                "The UI was done with React and Redux.  The backend was done with Python/Django REST Framework.  The database was Postgres.  All of these ran on an AWS EC2 instance via Docker Compose.  Nginx was used as a router/gateway."
            ],
            tags: [
                "typescript",
                "react",
                "redux",
                "sass",
                "bootstrap",
                "python",
                "django",
                "postgres",
                "docker",
                "aws",
                "nginx",
                "photoshop",
                "design",
                "development",
                "live"
            ]
        }, {
            name: "Jenna Lilley",
            codeUrl: "https://gitlab.com/adamski52/jenna-lilley",
            thumbnailUrl: "https://gitlab.com/adamski52/jenna-lilley/-/raw/master/thumbnail.jpg",
            summary: "A site made for Jenna Lilley, softball superstar and sister-in-law, to allow her to setup and register participants for hitting/fielding lessons.",
            description: [
                "Users were federated from a Facebook or Google account.  The backend was written in TypeScript/NodeJS, coordinating with a PostgreSQL Database and a TypeScript/React front end."
            ],
            tags: [
                "typescript",
                "react",
                "jest",
                "sass",
                "bootstrap",
                "html",
                "nodejs",
                "postgres",
                "aws",
                "docker",
                "npm",
                "photoshop",
                "illustrator",
                "design",
                "development",
                "leadership"
            ]
        }, {
            name: "Floating Islands",
            liveUrl: "http://jonathanadamski.com/archive/islands",
            codeUrl: "https://gitlab.com/adamski52",
            thumbnailUrl: "https://gitlab.com/adamski52/islands/-/raw/master/thumbnail.jpg",
            summary: "A controlled randomness / procedural generation project to draw various floating islands within a set of parameters.",
            description: [
                "TypeScript/React was used as the frontend.  Animations are controlled via CSS transitions.  No database or backend services were necessary."
            ],
            tags: [
                "typescript",
                "react",
                "jest",
                "sass",
                "pixijs",
                "npm",
                "design",
                "development",
                "live"
            ]
        }, {
            name: "Chicken Cross",
            codeUrl: "https://gitlab.com/adamski52/chickens",
            thumbnailUrl: "https://gitlab.com/adamski52/chickens/-/raw/master/thumbnail.jpg",
            summary: "My first experimentation with the Unity game engine.",
            description: [
                "Help the chicken cross the road.  Tiles are placed according to scene (levels of the game).  Each tile has a different strength meaning that the chicken may step on only a certain number of times. The idea was for it to be a bit like Frogger, except with punishments for indecisiveness.",
                "Unity is a AAA, full-feature game engine.  The code written for the game is C#.  Assets for the game are created as sprites (with Photoshop) or as 3D models (with Blender)."
            ],
            tags: [
                "unity",
                "c-sharp",
                "blender",
                "design",
                "development",
                "leadership"
            ]
        }, {
            name: "Procedural City",
            liveUrl: "http://jonathanadamski.com/archive/procedural-city",
            codeUrl: "https://gitlab.com/adamski52/procedural-city",
            thumbnailUrl: "https://gitlab.com/adamski52/procedural-city/-/raw/master/thumbnail.jpg",
            summary: "A procedural generated city using various rules for governing building size and style",
            description: [
                "Using TypeScript and the PixiJS canvas/svg library, generate a city block using rules to govern the composition of the block.  For instance, an airport cannot appear with a city.  Skyscrapers cannot be next to single family homes, etc.",
                "This is functionally useless but was learned as a way to learn about procedural generation techniques."
            ],
            tags: [
                "typescript",
                "react",
                "jest",
                "sass",
                "pixijs",
                "npm",
                "design",
                "development",
                "live"
            ]
        }, {
            name: "Germ Warfare",
            liveUrl: "http://jonathanadamski.com/archive/germ-warfare",
            codeUrl: "https://gitlab.com/adamski52/germ-battle",
            thumbnailUrl: "https://gitlab.com/adamski52/germ-battle/-/raw/master/thumbnail.jpg",
            summary: "Random germ warfare.  Let the strongest germ win.",
            description: [
                "On a flight from Detroit to Baltimore (about 2 hours), I was bored and so I created this little Game of Life type game where randomly placed pods of germs fight each other.",
                "At the borders, germs fight each other based on their neighbors.  Each germ has health and score damage against (or heal) surrounding germs based on this health and their team/color.",
                "Letting this play out for a while tends to result in a couple of germ groups that are too powerful to effectively budge each other.",
                "This was done with TypeScript and the PixiJS canvas/svg library."
            ],
            tags: [
                "typescript",
                "travis",
                "design",
                "development",
                "live"
            ]
        }, {
            name: "Vanilla REST Service",
            codeUrl: "https://gitlab.com/adamski52/vanilla-api",
            thumbnailUrl: "https://gitlab.com/adamski52/vanilla-api/-/raw/master/thumbnail.jpg",
            summary: "Make a security-enabled REST API without the use of any external libraries",
            description: [
                "A sociopathic job interview with no respect for people's time asked me to create a full featured REST API in NodeJS specifically, without the use of any external libraries at all.",
                "Check the package.json -- literally no dependencies.  Not even for a testing library -- I had to write my own.  For a job interview.  I should have told them 'no thanks'.  Maybe that's what they were really testing me on.",
                "But, I did it.  I even took into consideration things like session tokens and collisions on md5 hashes.  They said it wasn't good enough because I forgot to ensure that the ID in the PUT url matched the ID in the PUT body.",
                "I mention this because if you're an employer and you do any of the above, please skip me -- I don't want to work for you.",
                "But, if you're a sane employer then give it a look -- this is very good code that I'm quite proud of despite the unforgivable sin of a simple oversight during the 30 hours of free time they violated."
            ],
            tags: [
                "javascript",
                "nodejs",
                "development"
            ]
        }, {
            name: "Wedding RSVP Site",
            liveUrl: "http://jonathanadamski.com/archive/wedding",
            codeUrl: "https://gitlab.com/adamski52/wedding",
            thumbnailUrl: "https://gitlab.com/adamski52/wedding/-/raw/master/thumbnail.jpg",
            summary: "Fully functional site for wedding information and online RSVP",
            description: [
                "We wanted to have a site so that our guests could register online and get information about our upcoming wedding.  This is that site.",
                "The front end is in React.  The backend is in Python/Django REST Framework.  The database is Postgres.  They all ran together via Docker containers on an AWS EC2 instance.  Nginx was used as a router/gateway."
            ],
            tags: [
                "typescript",
                "react",
                "redux",
                "sass",
                "bootstrap",
                "python",
                "django",
                "postgres",
                "docker",
                "aws",
                "nginx",
                "photoshop",
                "design",
                "development",
                "live"
            ]
        }, {
            name: "Wedding 'Save the Date's",
            codeUrl: "https://gitlab.com/adamski52/wedding-save-the-date",
            thumbnailUrl: "https://gitlab.com/adamski52/wedding-save-the-date/-/raw/master/thumbnail.jpg",
            summary: "I'm not supposed to abbreviate this but I think Wedding STDs sounds a lot better.",
            description: [
                "For our wedding, I designed these 'Save the Date' postcards.  All design work is my own."
            ],
            tags: [
                "photoshop",
                "illustrator",
                "design"
            ]
        }, {
            name: "Wedding Invitations",
            codeUrl: "https://gitlab.com/adamski52/wedding-invites",
            thumbnailUrl: "https://gitlab.com/adamski52/wedding-invites/-/raw/master/thumbnail.jpg",
            summary: "Sorry you weren't invited",
            description: [
                "For our wedding, I designed these invitations.  They were packed into a burlap envelope, stacked vertically like a tabbed file folder.  All design work is my own."
            ],
            tags: [
                "photoshop",
                "illustrator",
                "design"
            ]
        }, {
            name: "Portfolio (2017)",
            codeUrl: "https://gitlab.com/adamski52/timeline-portfolio",
            thumbnailUrl: "https://gitlab.com/adamski52/timeline-portfolio/-/raw/master/thumbnail.jpg",
            summary: "My online activity represented as a single timeline",
            description: [
                "This site crawled GitHub APIs to learn about my commit history in order to populate a timeline of what I worked on.  The site would automatically detect what project was worked on, when it was worked on, and would present information about the project such as which languages were used.",
                "It was my first experiment with Angular 2+ and is one of my more ambitions personal projects in terms of how much effort it required."
            ],
            tags: [
                "angular",
                "typescript",
                "karma",
                "jasmine",
                "sass",
                "bootstrap",
                "photoshop",
                "html",
                "design",
                "development"
            ]
        }, {
            name: "Portfolio (2015)",
            liveUrl: "http://jonathanadamski.com/archive/portfolio-2015",
            codeUrl: "https://gitlab.com/adamski52/portfolio-2015",
            thumbnailUrl: "https://gitlab.com/adamski52/portfolio-2015/-/raw/master/thumbnail.jpg",
            awardsUrls: [{
                where: "Awwwards",
                url: "https://www.awwwards.com/sites/portfolio-of-jonathan-adamski"
            }],
            summary: "My 2015 Portfolio site -- a bigger undertaking.",
            description: [
                "This site was pretty cool.  Each section was represented by a portion of the atmosphere, from space to underground.  Many little easter eggs only I will ever know about are present -- such as Orion's belt and Cerebro from X-Men.",
                "At the time this site was advanced enough that I submitted it for awards consideration.  I was nominated but did not win, and my ratings have since crashed through the floor since it's so old and clunky by today's standards, but it was pretty cool."
            ],
            tags: [
                "angular",
                "javascript",
                "less",
                "photoshop",
                "html",
                "design",
                "development",
                "live"
            ]
        }, {
            name: "Portfolio (2013)",
            liveUrl: "http://jonathanadamski.com/archive/portfolio-2013",
            codeUrl: "https://gitlab.com/adamski52/portfolio-2013",
            thumbnailUrl: "https://gitlab.com/adamski52/portfolio-2013/-/raw/master/thumbnail.jpg",
            awardsUrls: [{
                where: "Awwwards",
                url: "https://www.awwwards.com/sites/jonathan-adamski"
            }],
            summary: "My 2013 Portfolio site -- back when jQuery and LESS were things.",
            description: [
                "Parallax motion drives a horizontal scrolling site which somehow still worked on mobile at the time.",
                "The theme was items hanging on a clothesline.  Scripting JavaScript was used (can you believe it??) to position elements randomly within different sections.  CSS animations were used to create a swinging effect.",
                "I submitted this site for awards consideration.  It did pretty OK.  Check it out."
            ],
            tags: [
                "javascript",
                "jquery",
                "photoshop",
                "html",
                "less",
                "design",
                "development",
                "live"
            ]
        }, {
            name: "Portfolio (2011)",
            liveUrl: "http://jonathanadamski.com/archive/portfolio-2011",
            codeUrl: "https://gitlab.com/adamski52/portfolio-2011",
            thumbnailUrl: "https://gitlab.com/adamski52/portfolio-2011/-/raw/master/thumbnail.jpg",
            summary: "My 2011 Portfolio site -- back when jQuery and LESS were things.",
            description: [
                "jQuery and plain CSS.  This site and the technologies were so simple, I did it all in a single file.",
                "Mobile was just becomming a thing to care about, testing was optional and I think this was my first site that stepped away from Flash."
            ],
            tags: [
                "javascript",
                "jquery",
                "photoshop",
                "html",
                "css",
                "design",
                "development",
                "live"
            ]
        }, {
            name: "Portfolio (2009)",
            codeUrl: "https://gitlab.com/adamski52/portfolio-2009",
            thumbnailUrl: "https://gitlab.com/adamski52/portfolio-2009/-/raw/master/thumbnail.jpg",
            summary: "My 2009 Portfolio site -- a goodbye to Flash.",
            description: [
                "Flash in all of its glory, prior to JavaScript taking over what was probably the best ECMA-based language of all time, ActionScript 3.",
                "This site was very heavily inspired by the video game World of Goo.  The planet featured 4 sections.  Clicking on one would rotate the planet to that section and present the page."
            ],
            tags: [
                "flash",
                "actionscript",
                "photoshop",
                "html",
                "css",
                "design",
                "development"
            ]
        }, {
            name: "Pet Project (Frontend)",
            codeUrl: "https://gitlab.com/adamski52/pet-project-client",
            thumbnailUrl: "https://gitlab.com/adamski52/pet-project-client/-/raw/master/thumbnail.jpg",
            summary: "My first experiment with Angular 2+",
            description: [
                "My future wife, girlfriend at the time, wanted to start a doggy daycare business.  At the time, I was living across the state and had lots of time, so I started work on a site which would let her manage all aspects of the business including scheduling, web cam services and billing.",
                "This is the front end of that site.  It is written using Angular 2 and coordinates with a backend API written with Python/Django."
            ],
            tags: [
                "typescript",
                "sass",
                "bootstrap",
                "gulp",
                "photoshop",
                "illustrator",
                "design",
                "development"
            ]
        }, {
            name: "Pet Project (Backend)",
            codeUrl: "https://gitlab.com/adamski52/pet-project-backend",
            thumbnailUrl: "https://gitlab.com/adamski52/pet-project-backend/-/raw/master/thumbnail.jpg",
            summary: "My first experiment with Django and Python",
            description: [
                "My future wife, girlfriend at the time, wanted to start a doggy daycare business.  At the time, I was living across the state and had lots of time, so I started work on a site which would let her manage all aspects of the business including scheduling, web cam services and billing.",
                "This is the backend service of that site.  It is written using Django REST framework and is backed by a MySQL database."
            ],
            tags: [
                "python",
                "django",
                "mysql",
                "bash",
                "development"
            ]
        }];
    }

    public getItems(filter: string) {
        if (!filter) {
            return this.items;
        }

        return this.items.filter((item) => {
            for (let i = 0; i < item.tags.length; i++) {
                if (this.getSimilarity(item.tags[i], filter) > this.matchThreshold) {
                    return true;
                }
            }
            return false;
        });
    }

    private getSimilarity(str1: string, str2: string) {
        let longer = str1,
            shorter = str2;

        if (str1.length < str2.length) {
            longer = str2;
            shorter = str1;
        }

        if (longer.length === 0) {
            return 1;
        }

        return (longer.length - this.getDistance(longer, shorter)) / longer.length;
    }

    private getDistance(str1: string, str2: string) {
        str1 = str1.toLowerCase();
        str2 = str2.toLowerCase();

        let costs = [];
        for (let i = 0; i <= str1.length; i++) {

            let lastValue = i;
            for (let j = 0; j <= str2.length; j++) {
                if (i === 0) {
                    costs[j] = j;
                    continue;
                }

                if (j > 0) {
                    let newValue = costs[j - 1];
                    if (str1.charAt(i - 1) !== str2.charAt(j - 1)) {
                        newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
                    }
                    costs[j - 1] = lastValue;
                    lastValue = newValue;
                }
            }

            if (i > 0) {
                costs[str2.length] = lastValue;
            }
        }

        return costs[str2.length];
    }
}