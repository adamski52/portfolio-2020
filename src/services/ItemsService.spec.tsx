import HttpService from "./HttpService";
import ItemsService from "./ItemsService";

const httpService = new HttpService("");
const itemsService = new ItemsService(httpService);

describe("ItemsService", () => {
    it("should get all items if no filter", () => {
        let actual = itemsService.getItems("");
        expect(actual.length).toEqual(18);
    });

    it("should get items exactly matching at least 1 filter", () => {
        let actual = itemsService.getItems("javascript");
        expect(actual.length).toEqual(4);
    });

    it("should get items fuzzy matching at least 1 filter", () => {
        let actual = itemsService.getItems("javascriz");
        expect(actual.length).toEqual(4);
    });

    it("should not get items that are too far off a match", () => {
        let actual = itemsService.getItems("javascrizzzz");
        expect(actual.length).toEqual(0);
    });
});