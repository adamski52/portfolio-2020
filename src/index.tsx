import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import RandomService from "./services/RandomService";
import ItemService from "./services/ItemsService";
import JunkService from "./services/JunkService";
import HttpService from "./services/HttpService";

const httpService = new HttpService("");
const randomService = new RandomService();
const itemsService = new ItemService(httpService);
const junkService = new JunkService(randomService)

ReactDOM.render(
    <React.StrictMode>
        <App randomService={randomService} junkService={junkService} itemsService={itemsService} />
    </React.StrictMode>,
    document.getElementById("root")
);
