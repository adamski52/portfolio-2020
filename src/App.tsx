import React from "react";
import NamePlate from "./components/NamePlate";
import SearchTextbox from "./components/SearchTextbox";
import IJunk from "./interfaces/junk";
import RandomService from "./services/RandomService";
import Junk from "./components/Junk";
import IItem from "./interfaces/item";
import ItemService from "./services/ItemsService";
import Item from "./components/Item";
import JunkService from "./services/JunkService";
import ActiveItem from "./components/ActiveItem";
import IStyle from "./interfaces/style";
import WigglyBox from "./components/WigglyBox";

export interface IAppProps {
    randomService: RandomService;
    itemsService: ItemService;
    junkService: JunkService;
}

export interface IAppState {
    seed: string;
    activeItem?: IItem;
    junks: IJunk[];
    items: IItem[];
    styles: {
        nameplate: IStyle;
        activeItem: IStyle;
        searchTextbox: IStyle;
        items: IStyle[];
    }
}

export default class App extends React.Component<IAppProps, IAppState> {
    private debounce: any;
    private initialSeed = "";
    private debounceMs = 1000;

    constructor(props: IAppProps) {
        super(props);

        props.randomService.setSeed(this.initialSeed);

        let items = this.props.itemsService.getItems(this.initialSeed)
        this.state = {
            seed: this.initialSeed,
            junks: this.props.junkService.getRandomJunk(),
            items: items,
            activeItem: undefined,
            styles: {
                nameplate: this.getRandomStyle(-2, 2),
                activeItem: this.getRandomStyle(-2, 2),
                searchTextbox: this.getRandomStyle(-2, 2),
                items: items.map((item) => {
                    return this.getRandomStyle(-2, 2)
                })
            }
        };

        this.onSetSeed = this.onSetSeed.bind(this);
        this.onItemFocus = this.onItemFocus.bind(this);
        this.onItemBlur = this.onItemBlur.bind(this);
    }

    private getRandomStyle(min:number, max:number) {
        return {
            style: {
                transform: "rotateZ(" + this.props.randomService.getRandomBetween(min, max) + "deg)"
            }
        };
    }

    private onItemFocus(item:IItem) {
        this.setState({
            activeItem: item
        });
    }

    private onItemBlur() {
        this.setState({
            activeItem: undefined
        });
    }

    private onSetSeed(seed: string) {
        this.props.randomService.setSeed(seed);

        this.setState({
            seed: seed
        }, () => {
            if (this.debounce) {
                clearTimeout(this.debounce);
            }

            this.debounce = setTimeout(() => {
                let junks = this.props.junkService.getRandomJunk();
                let items = this.props.itemsService.getItems(this.state.seed);
                this.setState({
                    junks: junks,
                    items: items,
                    styles: {
                        nameplate: this.getRandomStyle(-2, 2),
                        activeItem: this.getRandomStyle(-2, 2),
                        searchTextbox: this.getRandomStyle(-2, 2),
                        items: items.map((item) => {
                            return this.getRandomStyle(-2, 2)
                        })
                    }
                });
            }, this.debounceMs);
        });
    }

    private renderJunks() {
        return this.state.junks.map((junk) => {
            return (
                <Junk key={junk.url} junk={junk} />
            );
        });
    }

    private renderItems() {
        return this.state.items.map((item, index) => {
            return (
                <WigglyBox className="" key={item.name} style={this.state.styles.items[index]}>
                    <Item key={item.name} onSetSeed={this.onSetSeed} onItemFocus={this.onItemFocus} item={item} randomService={this.props.randomService} />
                </WigglyBox>
            );
        });
    }

    private renderActiveItem() {
        if(!this.state.activeItem) {
            return null;
        }

        return (
            <div className="active-item-backdrop">
                <WigglyBox style={this.state.styles.activeItem} className="offset-1 col-10 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-lg-3 col-lg-6 active-item-container">
                    <ActiveItem onItemBlur={this.onItemBlur} item={this.state.activeItem} />
                </WigglyBox>
            </div>
        );
    }

    public render() {
        return (
            <>
                <div className="container-fluid">
                    <div className="row">
                        <WigglyBox className="
                            col-12
                            offset-sm-1 col-sm-10
                            offset-md-2 col-md-8
                            offset-lg-3 col-lg-6
                            name-plate-container
                        " style={this.state.styles.nameplate}>
                            <NamePlate />
                        </WigglyBox>
                    </div>
                    <div className="row">
                        <WigglyBox className="
                            offset-1 col-11
                            offset-sm-2 col-sm-9
                            offset-md-3 col-md-7
                            offset-lg-4 col-lg-5
                            search-textbox-container
                        " style={this.state.styles.searchTextbox}>
                            <SearchTextbox onSetSeed={this.onSetSeed} seed={this.state.seed} />
                        </WigglyBox>
                    </div>
                    <div className="row">
                        <div className="
                            col-12
                            offset-sm-1 col-sm-10
                            offset-md-2 col-md-8
                            offset-lg-3 col-lg-6
                            item-container
                        ">
                            {this.renderItems()}
                        </div>
                    </div>
                </div>

                <div className="junk-container">
                    {this.renderJunks()}
                </div>

                {this.renderActiveItem()}
            </>
        );
    }
}
